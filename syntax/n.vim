" Vim syntax file
" Language: The N Programming Language
" Maintainer: Victor Scott Cushman
" Latest Revision: 19 January 2019

if exists("b:current_syntax")
  finish
endif

syn match nSingleLineComment "#.*$"

syn keyword nBasicKeyword let
syn keyword nBasicKeyword func
syn keyword nBasicKeyword struct
syn keyword nBasicKeyword variant
syn keyword nBasicKeyword union
syn keyword nBasicKeyword enum
syn keyword nBasicKeyword return
syn keyword nBasicKeyword alias
syn keyword nBasicKeyword true
syn keyword nBasicKeyword false
syn keyword nBasicKeyword null
syn keyword nBasicKeyword mut
syn keyword nBasicKeyword unique
syn keyword nBasicKeyword volatile
syn keyword nBasicKeyword as
syn keyword nBasicKeyword countof
syn keyword nBasicKeyword sizeof
syn keyword nBasicKeyword typeof
syn keyword nBasicKeyword import
syn keyword nBasicKeyword export
syn keyword nBasicKeyword introduce
syn keyword nBasicKeyword break
syn keyword nBasicKeyword continue
syn keyword nBasicKeyword in
syn keyword nBasicKeyword isolate

syn keyword nConditional if
syn keyword nConditional elif
syn keyword nConditional else
syn keyword nConditional loop
syn keyword nConditional defer

syn keyword nBuiltinType void
syn keyword nBuiltinType u8
syn keyword nBuiltinType u16
syn keyword nBuiltinType u32
syn keyword nBuiltinType u64
syn keyword nBuiltinType u128
syn keyword nBuiltinType u
syn keyword nBuiltinType s8
syn keyword nBuiltinType s16
syn keyword nBuiltinType s32
syn keyword nBuiltinType s64
syn keyword nBuiltinType s128
syn keyword nBuiltinType s
syn keyword nBuiltinType f16
syn keyword nBuiltinType f32
syn keyword nBuiltinType f64
syn keyword nBuiltinType f128
syn keyword nBuiltinType bool
syn keyword nBuiltinType ascii

syn match nLiteralDec  "\<[+]\?\d\+\(u8\|u16\|u32\|u64\|u128\|u\)"
syn match nLiteralDec "\<[+-]\?\d\+\(s8\|s16\|s32\|s64\|s128\|s\)"

syn match nLiteralHex  "\<[+]\?\(0x\|0X\)\x\+\(u8\|u16\|u32\|u64\|u128\|u\)"
syn match nLiteralHex "\<[+-]\?\(0x\|0X\)\x\+\(s8\|s16\|s32\|s64\|s128\|s\)"

syn match nLiteralOct  "\<[+]\?\(0o\|0O\)\x\+\(u8\|u16\|u32\|u64\|u128\|u\)"
syn match nLiteralOct "\<[+-]\?\(0o\|0O\)\x\+\(s8\|s16\|s32\|s64\|s128\|s\)"

syn match nLiteralBin  "\<[+]\?\(0b\|0B\)[01]\+\(u8\|u16\|u32\|u64\|u128\|u\)"
syn match nLiteralBin "\<[+-]\?\(0b\|0B\)[01]\+\(s8\|s16\|s32\|s64\|s128\|s\)"

syn match nLiteralFloat "\<[+-]\?\d\+\.\d\+\(f16\|f32\|f64\|f128\)"
syn match nLiteralFloat "\<[+-]\?\d\+\.\d\+\(e\|E\)[+-]\?\d\+\(f16\|f32\|f64\|f128\)"

syn match nLiteralASCII "'[^\\]'a"
syn match nLiteralASCII "'[^']*'a"

syn region nLiteralString start=+"+ skip=+\\\\\|\\"+ end=+"a+

let b:current_syntax = "n"

hi def link nSingleLineComment Comment
hi def link nBasicKeyword      Statement
hi def link nConditional       Statement
hi def link nBuiltinType       Type
hi def link nLiteralDec        Constant
hi def link nLiteralHex        Constant
hi def link nLiteralOct        Constant
hi def link nLiteralBin        Constant
hi def link nLiteralFloat      Constant
hi def link nLiteralASCII      Character
hi def link nLiteralString     Constant
