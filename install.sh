set -e
set -x

mkdir -p ~/.vim/syntax
cp syntax/n.vim ~/.vim/syntax/

mkdir -p ~/.vim/ftdetect
cp ftdetect/n.vim ~/.vim/ftdetect/
